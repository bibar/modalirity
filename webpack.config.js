var path = require('path');
var webpack = require('webpack');
module.exports = {
  entry: ['./index.js'],
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/dist/',
    filename: 'index.js'
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style/useable!css' // required to write 'require('./style.css') use and unuse'
      }, {
        test: /\.woff$/,
        loader: 'url-loader?prefix=font/&limit=5000&mimetype=application/font-woff'
      }, {
        test: /\.ttf$/,
        loader: 'file-loader?prefix=font/'
      }, {
        test: /\.eot$/,
        loader: 'file-loader?prefix=font/'
      }, {
        test: /\.svg$/,
        loader: 'file-loader?prefix=font/'
      }, {
        test: /\.gif$/,
        loader: 'url-loader?limit=10000&mimetype=image/gif'
      }, {
        test: /\.jpg$/,
        loader: 'url-loader?limit=10000&mimetype=image/jpg'
      }, {
        test: /\.png$/,
        loader: 'url-loader?limit=10000&mimetype=image/png'
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      // Automtically detect jQuery and $ as free var in modules
      // and inject the jquery library
      // This is required by many jquery plugins
      jQuery: "jquery",
      $: "jquery"
    })
  ]
};