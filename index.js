var h = {
  extend: function (obj1, obj2) {
    for (var attrname in obj2) { obj1[attrname] = obj2[attrname]; }
    return obj1;
  },
  parseHtml: function(str) {
    var tmp = document.implementation.createHTMLDocument();
    tmp.body.innerHTML = str;
    return tmp.body.children[0];
  },
  one: function (elem, event, cb) {
    elem.removeEventListener(event, cb);
    elem.addEventListener(event, cb);
  },
  prefixedEvent: function (element, type, callback) {
    var pfx = ["webkit", "moz", "MS", "o", ""];
    for (var p = 0; p < pfx.length; p++) {
      if (!pfx[p]) type = type.toLowerCase();
      element.removeEventListener(pfx[p]+type, callback, false);
      element.addEventListener(pfx[p]+type, callback, false);
    }
    pfx = null;
  }
};

var style = require('./index.css');
var Modalirity = function (id, conf) {
  if (!id) throw('You need set an id to your modal.');
  if (!conf) throw('You need set the required options.');

  var _template;

  var that = h.extend({
    id: id,
    content: '<button class="md-close">Close me!</button>',
    overlayColor: 'rgba(143,27,15,0.8)',
    useDefaultStyle: true,
    clickOnOverlayToClose: true,
    effect: 'md-anim-fade',
    beforeShow: function () {

    },
    afterShow: function () {

    }
  }, conf);

  var _init = function () {
    _template = h.parseHtml('<div id="' + id + '">'+
        '<div class="md-modal ' + that.effect + '">'+
          '<div class="md-content"></div>' + 
        '</div>'+
        '<div class="md-overlay" style="background: ' + that.overlayColor + '"></div>' + 
      '</div>');
    if (typeof that.content === 'string') {
      that.content = h.parseHtml(that.content);
    }
    _template.querySelector('.md-content').appendChild(that.content);
  };

  var _initClickEvents = function () {
    var overlay = _template.querySelector( '.md-overlay' );
    var modal = _template.querySelector( '.md-modal' );
    [].slice.call( document.querySelectorAll( '.md-trigger[data-modal="' + that.id + '"]' ) ).forEach( function( el, i ) {
      h.one(el, 'click', function( ev ) {
        _show( overlay, modal );
      });
    });
  };

  var _show = function (overlay, modal) {
    var closeBtn = modal.querySelector( '.md-close' );
    that.beforeShow();

    //esto no funciona, odio css3 a mano para animar, aguante velocity
    //h.prefixedEvent(modal, 'AnimationEnd', that.afterShow);
    
    document.body.appendChild(_template);

    setTimeout(function () {
      modal.classList.add( 'md-show' );
      that.afterShow();
    }, 0);

    if (closeBtn) {
      h.one(closeBtn, 'click', function( ev ) {
        ev.stopPropagation();
        _hide( modal );
      });
    }
    if (that.clickOnOverlayToClose) {
      h.one(overlay, 'click', function( ev ) {
        _hide( modal );
      });
    }
  };

  var _hide = function (modal) {
    modal.classList.remove( 'md-show' );
  }

  that.show = function () {
    var overlay = _template.querySelector( '.md-overlay' );
    var modal = _template.querySelector( '.md-modal' );
    _show( overlay, modal );
    return this;
  };

  that.hide = function () {
    var modal = _template.querySelector( '.md-modal' );
    _hide( modal );
    return this;
  };

  that.update = function () {
    if (that.useDefaultStyle) {
      style.use();
    } else {
      style.unuse();
    }
    _init();
    _initClickEvents();
    return this;
  };
  
  return that.update();
};

module.exports = Modalirity;